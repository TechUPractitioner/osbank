// Para poder cifrar, importamos el fichero que contiene la librería bcrypt
const crypt = require('../crypt');

// Importamos la librería 'request-json' para gestionar el Cliente Http
const requestJson = require('request-json');

// Importante finalizar la URL Base con la barra /
const baseMLabURL = "https://api.mlab.com/api/1/databases/osbank-ocdg/collections/";

//const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const mLabAPIKey = "apiKey=Ihl1LXLHw-kXBqLfUhYBgF86-TIDtgHh";

// Login de un Cliente accediendo al Backend en MLAB
function loginClient(req, res) {
  console.log("AuthController-->loginClient");  
  console.log("POST /osbank/mlab/login");  

  var httpClient = requestJson.createClient(baseMLabURL);
  var autenticado = false;
  var email = req.body.email;

  // La variable "id" tendrá el id del cliente en caso de que el Login sea correcto.
  var id = "";
  var first_name = "";

  var query = 'q={"email": "' + email + '"}';


  httpClient.get("clients?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {

      // muestra el array body por consola, para poder ver qué valores contiene
      //console.log(JSON.stringify(body));

      if (err) {
        var response = {
          "msg" : "Error obteniendo Cliente"
        }
        res.status(500);
      } else {

        // Si existe el usuario, tenemos que verificar que la password es correcta
        if (body.length > 0) {
          console.log("Vamos a validar si la passsword enviada en el Body coincide con la hasheada que está almacenada en MLAB");

          // Password correcta
          if (crypt.checkPassword(req.body.password, body[0].password)){
           console.log("La password es correcta");
           autenticado = true;
           id = body[0].id;
           first_name = body[0].first_name;

           // Tenemos que añadir un campo que indique que el usuario está logado.
           var putBody = '{"$set" : {"logged" : true}}';
           var query2 = 'q={"id": ' + id + '}';

           httpClient.put("clients?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
                console.log("estamos dentro del segundo httpCliente");
            }
           )
          } else {
            // Password incorrecta
            console.log("Las credenciales no coinciden. Usuario no autorizado");
            res.status(401);
          }
        } else {
          // El usuario no existe
          var response = {
            "msg" : "Cliente no encontrado"
          }
          res.status(404);
        }
      }

      var msg = autenticado ?
        "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

      var response = {
          "mensaje": msg,
          "id": id,
          "first_name" : first_name,
          "usuario": email,
          "isLogged": autenticado
        };

      console.log("response = " + JSON.stringify(response));
      //console.log("res.status = " + res
      res.send(response);
    }
  );

}

// // Logout 
function logoutClient(req, res) {
 console.log("POST /osbank/mlab/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 var httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("clients?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("El usuario existe y quiere cerrar la sesión");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("La consulta es " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("clients?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT realizado");
           var response = {
             "msg" : "El Cliente ha cerrado su sesión",
             "idCliente" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}

module.exports.loginClient = loginClient;
module.exports.logoutClient = logoutClient;
