// Como vamos a hacer uso de io, tenemos que importarlo en este archivo .js, referenciándolo ahora con '..'
const io = require('../io');

// Para poder cifrar, importamos el fichero que contiene la librería bcrypt
const crypt = require('../crypt');

// Importamos la librería 'request-json' para gestionar el Cliente Http
const requestJson = require('request-json');

// Importante finalizar la URL Base con la barra /
const baseMlabURL = "https://api.mlab.com/api/1/databases/osbank-ocdg/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
//const mLabAPIKey = "apiKey=Ihl1LXLHw-kXBqLfUhYBgF86-TIDtgHh"

// Listado de Clientes en el Backend de Mongo DB
function getClients(req, res) {
  console.log("GET /osbank/mlab/clients");
  console.log("Entramos en getClients");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

// resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
  httpClient.get("clients?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo clientes"
      }

      res.send(response);
    }
  )
}

// Obtención de un cliente por su Id
function getClientById(req, res) {
 console.log("GET /osbank/mlab/clients/:id");

 // resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
 var httpClient = requestJson.createClient(baseMlabURL);
 console.log("Client created");

 var id = req.params.id;
 console.log("Id del Cliente a traer " + id);
 var query = 'q={"id":' + id + '}';

 httpClient.get("clients?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     if (err) {
       var response = {
         "msg" : "Error obteniendo cliente"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         var response = body[0];
       } else {
         var response = {
           "msg" : "Cliente no encontrado"
         }
         res.status(404);
       }
     }

     res.send(response);
   }
 )
}

// Obtención del Id del último cliente
function lastId(req, res) {
  console.log("ClientController-->lastId");   

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var query = 's={"id": -1}';
  
// resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
  httpClient.get("clients?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {

      console.log("resMLab = " + resMLab);
      console.log("body = " + body);


      if (err) {
        var response = {
          "msg" : "Error obteniendo el Id"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = {
            "msg": "id obtenido",
            "id": body[0].id
          }; 
        }
      }

      console.log("response = " + response);  

      res.send(response);
    }
  )
}

// Función para dar de alta un cliente utilizando como Backend Mongo DB
function createClient(req,res) {
  console.log("ClientController-->createClient");  
  console.log("POST /osbank/mlab/clients");

  var id = parseInt(req.body.id) + 1;
  var first_name = req.body.first_name;

  console.log("first_name = " + first_name);

  var newClient = {
    "id" : id,
    "first_name" : first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.post("clients?" + mLabAPIKey, newClient,
    function(err, resMLab, body) {
      console.log("Cliente dado de alta correctamente");
      // status 201 significa "creado"
      var mensaje = {
        "msg" : "Cliente dado de alta correctamente"
      };


      var response = {
        "mensaje": mensaje,
        "id": id,
        "first_name" : first_name,
        "isLogged": true
      };

      res.status(201);
      res.send(response);
    }
  )
}





/***** EXPORTAMOS  ****************/
module.exports.createClient = createClient;
module.exports.lastId = lastId;
//module.exports.getClients = getClients;
//module.exports.getClientById = getClientById;
//module.exports.deleteClient = deleteClient;