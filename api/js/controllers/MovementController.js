const accountController = require('./AccountController.js');

// Importamos la librería 'request-jason'
const requestJson = require('request-json');

// Importante finalizar la URL Base con la barra /
const baseMLabURL = "https://api.mlab.com/api/1/databases/osbank-ocdg/collections/";


const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
//const mLabAPIKey = "apiKey=Ihl1LXLHw-kXBqLfUhYBgF86-TIDtgHh"


function getMovementsByIban(req, res) {

  console.log("MovementControllers.getMovementsByIban");
 
  console.log("GET osbank/mlab/movements/" + req.params.iban);

 var iban = req.params.iban;
 var query = 'q={"iban_origen":' + iban + '}';

 console.log("query = " + query);

 // resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
 var httpClient = requestJson.createClient(baseMLabURL);
 
  httpClient.get("movements?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
    
     if (err) {
       var response = {
         "msg" : "Error obteniendo los movimientos"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         var response = body;
       } else {
         var response = {
           "msg" : "iban no encontrado"
         }
         res.status(404);
       }
     }

     console.log("response = " + response);
     res.send(response);
   }
 );
}


// Función para dar de alta un movimiento asociado una cuenta determinada, ejecutando en el Server Node JS
function createMovement(req,res) {

  console.log("MovementControllers.createMovement");
  console.log("POST /osbank/mlab/movements");

  console.log("iban origen es " + req.body.iban_origen);
  console.log("iban destino es " + req.body.iban_destino);  
  console.log("importe es " + req.body.amount);
  console.log("saldo en cuenta:  " + req.body.balance);  
  console.log("el concepto es: " + req.body.description)

  // creamos el Object newMovement
  var newMovement = {
    "iban_origen" : req.body.iban_origen,
    "iban_destino" : req.body.iban_destino,
    "description" : req.body.description,
    "amount" : req.body.amount
  };

  // Variable en la que se almacenará la respuesta que se devuelva al navegador
  var response;

  console.log("query = " + baseMLabURL + "movements?" + mLabAPIKey);

  var httpClient = requestJson.createClient(baseMLabURL);
  
  // Persistimos el movimiento en MLAB
  httpClient.post("movements?" + mLabAPIKey, newMovement,
    function(err, resMLab, body) {
      console.log("Movimiento dado de alta correctamente");
      // status 201 significa "creado"
 
      // Actualizamos el saldo en la cuenta iban_origen
      var balance = req.body.balance.replace(".","");
      balance = balance.replace(",", ".");
      console.log("parseFloat(balance) = " + parseFloat(balance));
      console.log("parseFloat(req.body.amount) = " + parseFloat(req.body.amount));
      var balance_actualizado = parseFloat(balance) - parseFloat(req.body.amount);

      console.log("balance_actualizado = " + balance_actualizado);

      var putBody = '{"$set" : {"balance" : ' + balance_actualizado + '}}';
      var query = 'q={"iban": "' + req.body.iban_origen + '"}';      

      // Persistimos el nuevo saldo en la cuenta iban_origen
      httpClient.put("accounts?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
      function(errPUT, resMLabPUT, bodyPUT) {
          console.log("actualizamos el saldo en cuenta del emisor");

          if(errPUT){
            console.log("ERROR AL ACTUALIZAR EL SALDO");
          }
          
      }
     );    


     /*   AQUI HAY QUE IMPLEMENTAR LA LÓGICA PARA QUE ACTUALICE EL SALDO EN LA CUENTA DESTINO */
     var query2 = 'q={"iban": "' + req.body.iban_destino + '"}';          
     httpClient.get("accounts?" + query2 + "&" + mLabAPIKey,
      function(errGET, resMLabGET, bodyGET) {
          console.log("Obtenemos el saldo del iban_destino");
          if (err) {
            response = {
              "msg" : "Error obteniendo la cuenta"
            }        
            res.status(500);
          } else {
            if (bodyGET.length > 0) {
              console.log("El iban_destino existe. Recuperamos el saldo");

              console.log("bodyGET = " + bodyGET);
              console.log(bodyGET);

              var balance_destino = parseFloat(bodyGET[0].balance) + parseFloat(req.body.amount);
              
              console.log("parseInt(bodyGET[0].balance) = " + parseFloat(bodyGET[0].balance));
              console.log("parseInt(req.body.amount) = " + parseFloat(req.body.amount));
              console.log("balance_destino = " + balance_destino);

              // Tenemos que actualizar el saldo en la cuenta destino
              var putBody2 = '{"$set" : {"balance" : ' + balance_destino + '}}';


              httpClient.put("accounts?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody2),
                function(errPUT2, resMLabPUT2, bodyPUT2) {
                  console.log("Dentro del PUT");
                }
              )

              //console.log("body[0] = " + body[0]);

              response = {
                "mensaje": "Balance actualizado correctamente"
              };            
            } 
          } 

          res.send(response); 
        }
     
     );
         
    }
  );     
}

module.exports.getMovementsByIban = getMovementsByIban;
module.exports.createMovement = createMovement;