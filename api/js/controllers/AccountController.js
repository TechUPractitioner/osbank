// Importamos la librería 'request-jason'
const requestJson = require('request-json');

// Importante finalizar la URL Base con la barra /
const baseMLabURL = "https://api.mlab.com/api/1/databases/osbank-ocdg/collections/";


const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
//const mLabAPIKey = "apiKey=Ihl1LXLHw-kXBqLfUhYBgF86-TIDtgHh"


// Función para recuperar todas las cuentas dadas de alta
function getAccounts(req, res) {

  console.log("AccountController.js-->getAccounts");
 
  console.log("GET /osbank/mlab/accounts");


 // resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
 var httpClient = requestJson.createClient(baseMLabURL);
 console.log("Client created");
 
  httpClient.get("accounts?" + mLabAPIKey,
   function(err, resMLab, body) {
    console.log("BODY " + body);
    console.log(body);    
     if (err) {
       var response = {
         "msg" : "Error obteniendo cuentas"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         var response = body;
       } 
     }
     res.send(response);
   }
 );
}

// Función que recupera todas las cuentas que pertenecen a un cliente concreto
function getAccountsById(req, res) {
  console.log("AccountController-->getAccountsById"); 
  console.log("GET /osbank/mlab/accounts");

 var id = req.params.id;
 console.log("Id del Cliente del que queremos consultar las cuentas " + id);
 var query = 'q={"userId":' + id + '}';
 console.log("query es " + query);

 // resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
 var httpClient = requestJson.createClient(baseMLabURL);
 console.log("Client created");
 
  httpClient.get("accounts?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
    console.log("BODY " + body);
    console.log(body);

    
     if (err) {
       var response = {
         "msg" : "Error obteniendo usuario"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         var response = body;
       } else {
         var response = {
           "msg" : "Usuario no encontrado"
         }
         res.status(404);
       }
     }
     res.send(response);
   }
 );
}

// Función que devuelve el saldo de una determinada cuenta a partir de su IBAN
function getAccountBalanceByIban(psIBAN) {

  console.log("AccountController-->getAccountBalanceByIban"); 

 var query = 'q={"iban": "' + psIBAN + '"}';
 console.log("query es " + query);

 var balance = 0;

 // resMLab tiene toda la información que nos devuelve la API. Si hay dudas sobre los resultados, hacer console.log de este objeto.
 var httpClient = requestJson.createClient(baseMLabURL);
 console.log("Client created");
 
  httpClient.get("accounts?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
    console.log(body);

    console.log("DENTRO");
    if (err) {
       console.log("ERROR");
       console.log(err);
       var response = {
         "msg" : "Error obteniendo la cuenta asociada al IBAN"
       }
       balance = null;
    } else {
        console.log("RECUPERAMOS EL SALDO DE LA CUENTA DESTINO");
        balance = body[0].balance;
    }
  }
 );

  return balance;
}


module.exports.getAccounts = getAccounts;
module.exports.getAccountsById = getAccountsById;
module.exports.getAccountBalanceByIban = getAccountBalanceByIban;

